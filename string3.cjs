

function string3(input_string=""){

    if (input_string !== ""){
        
        let temp = input_string.split("/");
        const months = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"];
        return months[temp[1]-1];
        

    }else{
        return "";
    }

}


module.exports = string3;
