
function titleCase(result_string){

    result_string = result_string.toLowerCase().split(' ');

    for (let index = 0; index < result_string.length; index++) {
      result_string[index] = result_string[index].charAt(0).toUpperCase() + result_string[index].slice(1); 

    }
    return result_string.join(' ');
    
}

function string4(input_obj = {}){
    if (input_obj !== ""){
        let result_string = "";

        for( let index in input_obj){
        
            result_string = result_string.concat(input_obj[index]," ");
        
        }   
        
        return titleCase(result_string.trim());
    }else{
        return "";
    }

}


module.exports = string4;
