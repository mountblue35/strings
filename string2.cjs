

function string2(input_string=""){

    if (input_string !== ""){
        
        for(let index in input_string){
            
            let ascii = input_string.charCodeAt(index);
            // console.log(ascii);
        
            if( ( ascii >= 48 && ascii <= 57 ) || ascii === 46   ){
                // console.log("valid");
                continue;
            }else{
                return [];
            }
        }
        let result = input_string.split(".");
        return result.map(Number);

    }else{
        return "";
    }

}


module.exports = string2;
