

function string1(input_string=""){
    if (input_string !== ""){
        
        input_string = input_string.replaceAll("$","");
        input_string = input_string.replaceAll(",","");

        // console.log(input_string);
        
        for(let index in input_string){
            
            let ascii = input_string.charCodeAt(index);
            // console.log(ascii);
        
            if( ( ascii >= 48 && ascii <= 57 ) || ascii === 36 || ascii === 44 || ascii === 46 || ascii === 45  ){
                // console.log("valid");
                continue;
            }else{
                return 0;
            }
        }
        return Number(input_string);


    }else{
        return "";
    }

}

module.exports = string1;
